package com.sunrise.determining_factor


import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.DecelerateInterpolator
import android.view.animation.RotateAnimation
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_spin.setOnClickListener {
            spinWheel()
        }
    }

    private fun spinWheel() {
        btn_spin.visibility = View.GONE
        cl_main.setBackgroundColor(Color.WHITE)
        val rr = Random()
        val rnd = rr.nextInt(360)
        val rotate = RotateAnimation(
            0F, (rnd + 720).toFloat(),
            RotateAnimation.RELATIVE_TO_SELF, 0.5f,
            RotateAnimation.RELATIVE_TO_SELF, 0.5f
        )
        rotate.duration = 3000
        rotate.fillAfter = true
        rotate.interpolator = DecelerateInterpolator()
        rotate.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                calculatePoint(rnd)
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        iv_wheel.startAnimation(rotate)
    }

    private fun calculatePoint(rnd: Int) {
        var initialPoint = 0
        var endPoint = 51
        var i = 0
        var res: String? = null
        do {
            if (rnd in (initialPoint + 1) until endPoint) {
                res = sectors[i]
            }
            initialPoint += 51
            endPoint += 51
            i++
        } while (res == null)
        tv_spinChoose.text = res
        cl_main.setBackgroundColor(Color.parseColor("#808080"))
        btn_spin.visibility = View.VISIBLE
    }
}

